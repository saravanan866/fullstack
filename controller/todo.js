const UserSchema = require('../model/todoschema')

const userRegistration = (req, res) => {
    if(req.body.name !='' && req.body.email_id !='' && req.body.mobile_no !='')
    {   
    var Userdata=[{
                'name':req.body.name,
                'email_id':req.body.email_id,
                'mobile_no':req.body.mobile_no,
                'status':'true'
            }]

            UserSchema.UserTable.create(Userdata).then((ress) => {
                res.send({ Status: true, StatusCode: 201, message: "Data was successfully inserted" })
            }).catch((err) => {
                if(err)
                {
                    res.send({ Status: false, StatusCode: 409, message: "Database error", data: err })
                }
            });
    }
    else
    {
        res.json({msg:'bed request',status:400})
    
    }
}


const CreateTodoTask = (req, res) => {
    if(req.body.task_name !='' && req.body.task_status !='' && req.body.user_id !='')
    {   
    var Tododata=[{
                'task_name':req.body.task_name,
                'task_status':req.body.task_status,
                'user_id':req.body.user_id,
                'create_date':new Date().toISOString(),
            }]

            
            UserSchema.TodoTable.create(Tododata).then((ress) => {
                res.send({ Status: true, StatusCode: 201, message: "Data was successfully inserted" })
            }).catch((err) => {
                if(err)
                {
                    res.send({ Status: false, StatusCode: 409, message: "Database error", data: err })
                }
            });
    }
    else
    {
        res.json({msg:'bed request',status:400})
    
    }
}



const UpdateTodoTask = (req, res) => {
    if(req.body.task_status !='' && req.body.todo_id !='')
    {   
    var Tododata={
                'task_status':req.body.task_status,
                'modified_date':new Date().toISOString(),
            }
          
            UserSchema.TodoTable.findByIdAndUpdate(req.body.todo_id,Tododata, 
                function(err, response_status){
                    if(response_status !="")
                    {
                        res.json({msg:'successfully updated',response_status})
                    }
                    else
                    {
                        res.json({msg:'updated faild',status:400})
                    }
                    
                });
    }
    else
    {
        res.json({msg:'bed request',status:400})
    
    }
}



const DeleteTodoTask = (req, res) => {
    if(req.body.todo_id !='')
    {   
        console.log(req.body.todo_id);
        UserSchema.TodoTable.findByIdAndRemove(req.body.todo_id, function(err, response){
                if(err)
                {
                 res.json({message: "Error",status:400});
                }
                else
                {
                    res.json({message: "successfully deleted",status:200});
                }
             });
    }
    else
    {
        res.json({msg:'bed request',status:400})
    
    }
}


const getTodoList = (req, res) => {
    UserSchema.TodoTable.find({user_id:req.body.user_id}, null,{sort: {'_id': -1}},function (err, docs) {
        if(err){
           console.log(err)
        }else{
             res.json(docs);
        }
     }); 
    }



 const userinfo = (req, res) => {
    UserSchema.UserTable.findOne({_id:req.body.user_id},function (err, docs) {
        if(err){
           console.log(err)
        }else{
             res.json(docs);
        }
     }); 
    }   

module.exports = {
    userRegistration:userRegistration,
    CreateTodoTask:CreateTodoTask,
    UpdateTodoTask:UpdateTodoTask,
    DeleteTodoTask:DeleteTodoTask,
    getTodoList:getTodoList,
    userinfo:userinfo
    }
    