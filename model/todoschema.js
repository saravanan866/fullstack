const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    name: { type: String, required: true },
    email_id: { type: String, required: true },
    mobile_no: { type: String, required: true },
    status: { type: String, required: true },
}, {strict: false})



const TodoSchema = new mongoose.Schema({
    task_name: { type: String, required: true },
    task_dedscription: { type: String,required: false},
    task_status: { type: String, required: true },
    user_id: { type: String, required: true },
    create_date: { type: String, required: true },
    modified_date: { type: String,required: false},

}, {strict: false})



mongoose.set('useFindAndModify', false);
module.exports =  {UserTable:mongoose.model('users', UserSchema),TodoTable:mongoose.model('todo_infos', TodoSchema)}
