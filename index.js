const express = require('express')
const app = express()
const UserSchema = require('./dbconfig')
var _ = require("lodash");
app.use(express.static(__dirname));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

var bodyParser = require('body-parser')
app.use( bodyParser.json() );   
app.use(bodyParser.urlencoded({   
  extended: true
}));

app.use(require('./route/route'))

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})

app.get('/', function(request, response){
    response.sendFile(__dirname+'/index.html');
});

