const express = require('express')
const app = express()
const router = express.Router()
const UserSchema = require('../dbconfig')
var _ = require("lodash");
const todo =require('../controller/todo')

router.post('/todo_user_registration',todo.userRegistration)
router.post('/create_todo',todo.CreateTodoTask)
router.post('/update_todo',todo.UpdateTodoTask)
router.post('/delete_todo',todo.DeleteTodoTask)
router.post('/get_todo',todo.getTodoList)
router.post('/user_info',todo.userinfo)

module.exports = router