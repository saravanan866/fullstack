$("#sortable").sortable();
$("#sortable").disableSelection();


onloadmode();
userinfo();


$('.add-todo').on('keypress',function (e) {
      e.preventDefault
      if (e.which == 13) {
           if($(this).val() != ''){
           var todo = $(this).val();
           $.ajax({
            url: "http://localhost:3000/create_todo", 
            method:'post',
            data:{task_name:todo,task_status:'pending',user_id:'5c42c372a9abc81228c4dea2'},
            success: function(result){
                    onloadmode()
                     $('.add-todo').val('');
              }});


           }
      }
});


function onloadmode()
{
    var htmls="";
    $.ajax({
    url: "http://localhost:3000/get_todo", 
    method:'post',
    data:{user_id:'5c42c372a9abc81228c4dea2'},
    success: function(result){

        $.each(result, function(key,value){
            if(value.task_status=="completed")
            {
                htmls += '<li class="ui-state-default"><div class="checkbox"><label><input type="checkbox"  value="'+value._id+'" onclick="task_status_update(this)" checked /><strike>'+ value.task_name +'</strike></label><i class="fa fa-remove icons" style="font-size:14px;color:red" onclick=task_remove(\"'+value._id+'\") ></i></div></li>';
            }
            else
            {
                htmls += '<li class="ui-state-default"><div class="checkbox"><label><input type="checkbox"  value="'+value._id+'" onclick="task_status_update(this)"/>'+ value.task_name +'</label><i class="fa fa-remove icons" style="font-size:14px;color:red" onclick=task_remove(\"'+value._id+'\") ></i></div></li>';   
            }


             
        });

        
        $('#sortable').html(htmls);
        countTodos();
                
    }});

}



function task_status_update(tag_info)
{
    if($(tag_info).prop('checked') == true){

           $.ajax({
            url: "http://localhost:3000/update_todo", 
            method:'post',
            data:{task_status:'completed',todo_id:$(tag_info).val()},
            success: function(result){
                    onloadmode()
                        
              }});

    }
    else
    {
          $.ajax({
            url: "http://localhost:3000/update_todo", 
            method:'post',
            data:{task_status:'pending',todo_id:$(tag_info).val()},
            success: function(result){
                    onloadmode()
                        
              }});

    }
}



function task_remove(tag_info)
{
            if (confirm('Are You Sure Remove this item ?')) {
                $.ajax({
                    url: "http://localhost:3000/delete_todo", 
                    method:'post',
                    data:{todo_id:tag_info},
                    success: function(result){
                    onloadmode()

                }});
            } 

}


function countTodos(){
    var count = $("#sortable li").length;
    $('.count-todos').html(count);
}


function userinfo(get_id)
{
    $.ajax({
        url: "http://localhost:3000/user_info", 
        method:'post',
        data:{user_id:'5c42c372a9abc81228c4dea2'},
        success: function(result){
        $('.user_name').html("<p>Username:"+result.name+"</p>")
    }});
}
